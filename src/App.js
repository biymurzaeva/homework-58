import {useState} from "react";
import './App.css';
import Modal from "./components/UI/Modal/Modal";
import Message from "./components/Message/Message";
import Alert from "./components/UI/Alert/Alert";

function App() {
  const [modalShow, setModalShow] = useState(true);

  const [alertShow, setAlertShow] = useState(true);

  const close = () => {
    setModalShow(false);
  };

  const alertClose = () => {
    setAlertShow(false);
  }

  return (
    <div className='container'>
      <Alert
        type="warning"
        dismiss={alertShow}
        show={alertShow}
        close={alertClose}
      >
        This is a warning type alert
      </Alert>
      <Modal
        show={modalShow}
        close={close}
        title="Some kinda modal title"
      >
        <Message/>
      </Modal>
    </div>
  );
}

export default App;
