import React from 'react';
import './Backdrop.css';

const Backdrop = ({show}) => {
	return show ? <div className="Backdrop"/> : null;
};

export default Backdrop;