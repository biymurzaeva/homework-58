import React from 'react';
import './Modal.css';
import Backdrop from "../Backdrop/Backdrop";

const Modal = props => {
	return (
		<>
			<Backdrop
				show={props.show}
			/>
			<div
				className="Modal"
				style={{
					transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
					opacity: props.show ? '1' : '0',
				}}
			>
				<p className="btn" onClick={e => props.close(e.target)}>x</p>
				<strong>{props.title}</strong>
				<hr/>
				{props.children}
			</div>
		</>
	);
};

export default Modal;