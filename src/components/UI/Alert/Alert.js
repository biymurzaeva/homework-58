import './Alert.css';

const Alert = props => {
	let btnClasses = ['Alert'];

	if (props.type) {
		btnClasses.push(props.type);
	}

	const stateBtn = () => {
		if (props.dismiss === undefined) {
			return '0';
		}
		else {
			return '1';
		}
	}

	return (
		<div
			className={btnClasses.join(' ')}
			style={{
				transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
				opacity: props.show ? '1' : '0',
			}}
		>
			<button
				className="btn"
				style={{opacity: stateBtn()}}
				onClick={e => props.close(e.target)}
			>
				x
			</button>
			{props.children}
		</div>
	);
};

export default Alert;